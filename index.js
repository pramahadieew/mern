'use strict'
require('dotenv').config();
const express = require('express');
const handle = require('./hadlers');
const app = express();

const cors = require('cors');
const bodyParser = require('body-parser');

const port = process.env.PORT;

app.use(cors());
app.use(bodyParser.json());
app.get('/', (req, res) => res.json({
    Nama: 'Pramahadi',
    Alamat: 'Disini kalau disana sama kamu',
    Status: "Dikit lagi ganti"
}));

app.use(handle.notFound);

app.use(handle.errors);

app.listen(port, () => {
    console.log(`Server started on ${port}`);
});